#  jforum.orize.starter

#### 介绍

jforum.orize的SpringBoot自动装配, 非SpringBoot也可以使用. orize.spring包中提供了SpringMVC中使用时用到的两个实现.使用示例：[jforum2的分支:boot-orize](https://gitee.com/subpu/jforum2/tree/boot-orize)

#### 软件架构

项目基于OpenJDK 11.x开发和编译. 使用的SpringBoot版本: 2.3.3.RELEASE, 这里的版本不重要您懂的


#### 安装教程

##### 1.  项目配置

项目的所有配置项:
```
spring.orize.loader = 资源定义的类型,支持:xml,json,anno
默认值:xml

spring.orize.annoPackage = 当loader=anno时扫描Orize注解所在的基础包名
默认值:null/手动创建资源

spring.orize.path = 当loader等于xml/json时的资源所在的位置,需要放到WEB-INF目录下
默认值:resources.xml

spring.orize.queryClass = 用户角色查询实现类全名(需要实现:OrizeMemberQuery)
必填项

spring.orize.roleMatch = 用户角色的匹配规则,支持: any(单角色匹配)，all(多角色匹配)
默认值:any

spring.orize.ignoreDomain = 忽略的域名, 只要请求来自这些域名不进行验证. 多个之间用逗号分隔, 可选项
默认值:null/没有忽略要求

spring.orize.ignorePath = 忽略的请求路径, 只要请求路径符合定义不进行验证. 多个之间用逗号分隔, 可选项
默认值:null/没有忽略要求

spring.orize.ignoreExt = 忽略的请求文件扩展名, 只要请求文件符合定义不进行验证. 多个之间用逗号分隔, 可选项
默认值:null/没有忽略要求

spring.orize.logPath=验证失败时的提示地址
默认值:/orize/result
```

##### 2. 实现OrizeMemberQuery
例:
```
package com.apobates.forum.orize.auth.impl;
/**
 * member.strategy.core.OrizeMemberQuery的实现
 */
public class OrizeMemberQueryImpl implements OrizeMemberQuery {}
```
在application.properties中的配置如下(手动资源:/WEB-INF/resources.xml)
```
# Orize Auth
spring.orize.queryClass=com.apobates.forum.orize.auth.impl.OrizeMemberQueryImpl
spring.orize.ignoreExt=js,css,png,svg,gif,jpg
```
##### 3.  定义资源

使用注解自动生成资源时需要在控制器方法上增加注解，手动创建资源需要把文件放到WEB-INF目录下
