package com.apobates.forum.orize.spring.boot;

import org.springframework.context.annotation.Import;
import java.lang.annotation.*;

/**
 * OrizeAuth注解方式装配
 * @author xiaofanku@live.cn
 * @since 20210821
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({OrizeSpringAutoConfiguration.class})
public @interface EnableOrizeAuthConfiguration {
}
