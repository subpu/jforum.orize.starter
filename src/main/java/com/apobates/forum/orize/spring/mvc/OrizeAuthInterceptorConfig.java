package com.apobates.forum.orize.spring.mvc;

import com.apobates.forum.orize.spring.OrizeAuthSpringInterceptorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 注册拦截器
 * @author xiaofanku
 * @since 20210821
 */
@Configuration
public class OrizeAuthInterceptorConfig implements WebMvcConfigurer{
    @Autowired
    @Qualifier("orizeAuthInterceptor")
    private OrizeAuthSpringInterceptorImpl orizeAuthInterceptor;
    private final static Logger logger = LoggerFactory.getLogger(OrizeAuthInterceptorConfig.class);

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Orize 请求地址验证
        registry.addInterceptor(orizeAuthInterceptor).addPathPatterns("/**");
        if(logger.isDebugEnabled()){
            logger.debug("[Orize.Starter]Spring HandlerInterceptorAdapter registed Finish");
        }
    }
}
