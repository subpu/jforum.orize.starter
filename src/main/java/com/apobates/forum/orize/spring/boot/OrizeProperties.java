package com.apobates.forum.orize.spring.boot;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Orize的配置参数类
 * @author xiaofanku@live.cn
 * @since 20210821
 */
@ConfigurationProperties(prefix = "spring.orize")
public class OrizeProperties {
    //资源定义的类型:xml,json,anno
    private String loader="xml";
    //当loader=anno时扫描Orize注解所在的基础包名
    private String annoPackage=null;
    //当loader时xml/json时的资源所在的位置,需要放到WEB-INF目录下
    private String path="resources.xml";
    //用户角色查询实现类全名(需要实现:OrizeMemberQuery)
    private String queryClass;
    //用户角色的匹配规则: any(单角色匹配)，all(多角色匹配)
    private String roleMatch="any";
    //忽略的域名, 只要请求来自这些域名不进行验证. 多个之间用逗号分隔, 可选项
    private String ignoreDomain=null;
    //忽略的请求路径, 只要请求路径符合定义不进行验证. 多个之间用逗号分隔, 可选项
    private String ignorePath=null;
    //忽略的请求文件扩展名, 只要请求文件符合定义不进行验证. 多个之间用逗号分隔, 可选项
    private String ignoreExt=null;
    //出错时的提示地址
    private String logPath="/orize/result";

    public String getLoader() {
        return loader;
    }

    public void setLoader(String loader) {
        this.loader = loader;
    }

    public String getAnnoPackage() {
        return annoPackage;
    }

    public void setAnnoPackage(String annoPackage) {
        this.annoPackage = annoPackage;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getQueryClass() {
        return queryClass;
    }

    public void setQueryClass(String queryClass) {
        this.queryClass = queryClass;
    }

    public String getRoleMatch() {
        return roleMatch;
    }

    public void setRoleMatch(String roleMatch) {
        this.roleMatch = roleMatch;
    }

    public String getIgnoreDomain() {
        return ignoreDomain;
    }

    public void setIgnoreDomain(String ignoreDomain) {
        this.ignoreDomain = ignoreDomain;
    }

    public String getIgnorePath() {
        return ignorePath;
    }

    public void setIgnorePath(String ignorePath) {
        this.ignorePath = ignorePath;
    }

    public String getIgnoreExt() {
        return ignoreExt;
    }

    public void setIgnoreExt(String ignoreExt) {
        this.ignoreExt = ignoreExt;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }
}
