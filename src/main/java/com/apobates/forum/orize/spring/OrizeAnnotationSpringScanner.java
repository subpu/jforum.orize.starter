package com.apobates.forum.orize.spring;

import com.apobates.forum.orize.core.OrizeAnnotationScanner;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 扫描SpringMVC控制器方法上的Orize注解
 * 使用:GetMapping,PostMapping,RequestMapping注解生成资源定义的相关信息
 * @author xiaofanku@live.cn
 * @since 20210821
 */
public class OrizeAnnotationSpringScanner extends OrizeAnnotationScanner {

    @Override
    protected List<String> parsePath(Method method) {
        Stream<String> _methodPath = Stream.empty(), _classPath = Stream.empty();
        GetMapping getAnno = method.getAnnotation(GetMapping.class);
        if(null != getAnno){
            String[] getMethodPath = getAnno.path();
            String[] getMethodValue = getAnno.value();

            _methodPath = Stream.concat(Stream.of(getMethodPath), Stream.of(getMethodValue));
        }
        PostMapping postAnno = method.getAnnotation(PostMapping.class);
        if(null != postAnno){
            String[] postMethodPath = postAnno.path();
            String[] postMethodValue = postAnno.value();

            _methodPath = Stream.concat(Stream.of(postMethodPath), Stream.of(postMethodValue));
        }
        //
        Class<?> aClass = method.getDeclaringClass();
        RequestMapping _crm = aClass.getAnnotation(RequestMapping.class);
        if(null != _crm){
            String[] classPathPrefix = _crm.path();
            String[] classValPrefix = _crm.value();
            _classPath = Stream.concat(Stream.of(classPathPrefix), Stream.of(classValPrefix));
        }

        return OrizeAnnotationScanner.cartesian(
                _methodPath.collect(Collectors.toList()),
                _classPath.collect(Collectors.toList()),
                OrizeAnnotationScanner.mergePathFun);
    }
}
