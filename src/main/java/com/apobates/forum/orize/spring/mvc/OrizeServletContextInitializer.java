package com.apobates.forum.orize.spring.mvc;

import com.apobates.forum.orize.core.OrizeAuthCommonHelper;
import com.apobates.forum.orize.servlet.OrizeServletContextListener;
import com.apobates.forum.orize.spring.boot.OrizeProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import javax.annotation.ManagedBean;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * 若使用注解生成资源文件的初始化器
 * @author xiaofanku@live.cn
 * @since 20210822
 */
@ManagedBean
public class OrizeServletContextInitializer implements ServletContextInitializer {
    @Autowired
    private OrizeProperties orizeProperties;
    private final static Logger logger = LoggerFactory.getLogger(OrizeServletContextInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        if(!OrizeAuthCommonHelper.RES_TYPE_ANNO.equalsIgnoreCase(orizeProperties.getAnnoPackage())){
            if(logger.isDebugEnabled()){
                logger.debug("[Orize.Starter]not used orize annotation generate resource");
            }
            return;
        }
        //生成Orize注解的资源文件
        OrizeServletContextListener orsc = new OrizeServletContextListener();
        servletContext.addListener(orsc);
        //OrizeAnnotationScanner的实现类
        servletContext.setInitParameter("orizeAnnoClass", "com.apobates.forum.orize.spring.OrizeAnnotationSpringScanner");
        //Orize注解所在包名
        servletContext.setInitParameter("orizeAnnoPackage", orizeProperties.getAnnoPackage());
        if(logger.isDebugEnabled()){
            logger.debug("[Orize.Starter]orize annotation generate resource Finish");
        }
    }
}
